import {createAppContainer} from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import React, { useState, useEffect } from "react";
import {Home} from "./src/screens/Home";
import {Player} from "./src/screens/Player";
import Artists from "./src/screens/Artists";

const Project= createMaterialBottomTabNavigator({
  Top: {
   screen: Home,
   
  },
  Player: {
   screen: Player
  },
  Artists: {
    screen: Artists
   }
},{
    initialRouteName: 'Top',
    activeColor: '#f0edf6',
    inactiveColor: '#3e2465',
    barStyle: { backgroundColor: '#694fad' }
  });
export default createAppContainer(Project);