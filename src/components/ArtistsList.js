import React from "react";
import { View,ScrollView, Text, FlatList, StyleSheet } from "react-native";
import { useNavigation } from 'react-navigation-hooks'
import { Item } from "./Item";

export const ArtistsList = ({data,title}) => {

  const { navigate } = useNavigation();
  const onItemPress = (title)=>{
    navigate('Player',{pageid:title,queryType:'titles'})
  }

  return (
    <ScrollView>
    <Text style={styles.listTitle}>{title}</Text>
      <FlatList
        data={data[0]}
        renderItem={({ item}) => (
          <Item title={item["title"]}
                date={item["timestamp"].split("T")[0]}
                onItemPress={()=>onItemPress(item['title'])}
                />

        )}
        keyExtractor={(item,index) => item.title}
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  listTitle: {
    flex: 1,
    backgroundColor:'#694fad',
    color:'#fff',
    fontSize: 15,
    marginTop: 30,
    fontWeight: "bold",
    textAlign: "center"
  }
});
