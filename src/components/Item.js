import React from "react";
import { View, TouchableOpacity, Text, StyleSheet } from "react-native";

import { Button } from "react-native-paper";
export const Item = ({ title, date, onItemPress,onArtistNamePress, ArtistName }) => {
  console.log('Item Component')
  return (
    <View style={ArtistName?styles.item:''}>
      {ArtistName?
      <TouchableOpacity  onPress={() => onArtistNamePress(ArtistName)}>
        <View>
          <Text style={styles.ArtistName}>{ArtistName}</Text>
        </View>
      </TouchableOpacity>
      :<Text></Text>
    
      }
      <TouchableOpacity style={styles.item} onPress={() => onItemPress()}>
        <View>
          <Text style={styles.date}>{date}</Text>
          <Text style={styles.title}>{title}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    backgroundColor: "#F0F8FF",
    borderRadius: 4,
    padding: 5,
    borderStyle: "solid",
    borderWidth:0.1,
    marginVertical: 8,
    marginHorizontal: 16
  },
  ArtistName: {
    fontSize: 18,
    margin: 10,
    
    textAlign: "center"
  },
  title: {
    fontSize: 15
  },
  date: {
    fontSize: 15,
    backgroundColor: "#ADD8E6",
    textAlign: "center"
  }
});
