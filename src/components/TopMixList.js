import React from "react";
import { View,ScrollView, Text, FlatList, StyleSheet } from "react-native";
import { useNavigation } from 'react-navigation-hooks'
import { Item } from "./Item";

export const TopMixList = ({data}) => {
  console.log('TopMixList')

  const { navigate } = useNavigation();
  const onItemPress = (pageid)=>{
    navigate('Player',{pageid,queryType:'pageids'})
  }
  const onArtistNamePress = (name)=>{
    navigate('Artists',{name,queryType:'pageids'})
  }


  return (
    <ScrollView>
      <Text style={styles.listTitle}>Resent mix sets</Text>
      <FlatList
        data={data[0]}
        renderItem={({ item }) => (
          <Item title={item["title"]}
                date={item["timestamp"].split("T")[0]}
                ArtistName = { item['title'].split(' - ')[1]}
                onItemPress={()=>onItemPress(item['pageid'])}
                onArtistNamePress ={onArtistNamePress}
                />

        )}
        keyExtractor={item => item["pageid"].toString()}
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  listTitle: {
    backgroundColor:'#694fad',
    color:'#fff',
    fontSize: 15,
    fontWeight: "bold",
    textAlign: "center"
  }
});
