import React, { useState, useEffect } from "react";
import { TopMixList } from "../components/TopMixList";
import { ActivityIndicator, SafeAreaView, StyleSheet } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';

export const Home =()=> {

  const [topMixes,setTopMixes] = useState([]);
  const [loading,setLoading] = useState(true);
  
  const getTopMixes = async ()=>{
    try {
      const data = new FormData();

      let url = "https://www.mixesdb.com/db/api.php?";

      data.append("action", "query");
      data.append("format", "json");
      data.append("list", "recentchanges");
      data.append("rcnamespace", "0");
      data.append("rclimit", "10");
      data.append("rctype", "new");

      await fetch(url, {
        method: "POST",
        body: data,
        mode: "cors",
        
      })
        .then(json => {
          return json.text();
        })
        .then(data => {
          const obj = JSON.parse(data);
          
 
            setTopMixes([...topMixes,obj.query.recentchanges])
            setLoading(false)

        });
    } catch (err) {
      console.log("Error fetching data---------->", err);
    }

  }

  useEffect( ()=>{
    if(topMixes.length===0){
      getTopMixes()

    }

  })
    let topMixList

    if(loading){
       topMixList = <ActivityIndicator size="large" color="#0000ff" />
    }else{
      topMixList =  <TopMixList data={topMixes}/>
    }
    return (
      <SafeAreaView style={styles.container}>
         {topMixList}
      </SafeAreaView>
    );
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
    backgroundColor: "#0000",
    alignItems: "center",
    justifyContent: "center"
  }
});


const myIcon = <Icon name="rocket" size={20} color="#fff" />;
Home.navigationOptions = () => ({
  title: "Top Mixes",
  tabBarIcon:myIcon,
  headerStyle: {
    backgroundColor: "#f4511e"
  },
  headerTintColor: "#fff",
  headerTitleStyle: {
    fontWeight: "bold"
  }
});
