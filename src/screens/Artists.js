import React, { useState, useEffect } from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import { ArtistsList } from "../components/ArtistsList";
import { ActivityIndicator, SafeAreaView, StyleSheet,View,Text } from "react-native";

export default Artist = ({ navigation }) => {
  const [artistsSets, setArtistsSets] = useState([]);



  const didBlurSubscription =navigation.addListener(
    'didFocus',
    payload => {
      if(payload.state!=artistsSets)
          setArtistsSets([]);
    }
  );

  const getArtistMixes = async () => {
    try {
      const data = new FormData();

      let url = "https://www.mixesdb.com/db/api.php?";

      let name = navigation.state.params.name;
      data.append("action", "query");
      data.append("format", "json");
      data.append("list", "search");
      data.append("formatversion", "2");
      data.append("srsearch", name);

      await fetch(url, {
        method: "POST",
        body: data,
        mode: "cors"
      })
        .then(json => {
          return json.text();
        })
        .then(data => {
          const obj = JSON.parse(data);
           setArtistsSets([...artistsSets,obj.query.search])
        });
    } catch (err) {
      console.log("Error fetching data---------->", err);
    }
  };

  useEffect(() => {
    if(artistsSets.length===0)
        getArtistMixes();

  });

  return (
    <SafeAreaView style={styles.container}>
      {
        navigation.state.params && navigation.state.params.name ?
        <ArtistsList data={artistsSets}  title={navigation.state.params.name}/>:
        <View>
          <Text>Please select an artist</Text>
        </View>
        // <ActivityIndicator size="large" color="#0000ff" />
      }
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#0000",
    alignItems: "center",
    justifyContent: "center"
  }
});

const myIcon = <Icon name="user-circle" size={20} color="#fff" />;
Artist.navigationOptions = () => ({
  title: "Artists Mixes",
  tabBarIcon: myIcon,
  headerStyle: {
    backgroundColor: "#f4511e"
  },
  headerTintColor: "#fff",
  headerTitleStyle: {
    fontWeight: "bold"
  }
});
