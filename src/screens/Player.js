import {
  ActivityIndicator,
  StyleSheet,
  WebView,
  View,
  Text
} from "react-native";
import { useNavigation, useNavigationParam } from "react-navigation-hooks";
import React, { useState, useEffect } from "react";
import Icon from "react-native-vector-icons/FontAwesome";

export const Player = ({ navigation }) => {
  const [source, setSource] = useState("");
  const [loading, setLoading] = useState(true);
  const [pageid, setPageid] = useState("");

  useEffect(() => {
    getExtLinks();
  });

  const getExtLinks = async () => {
    try {
      const data = new FormData();
      let pageId = navigation.state.params.pageid;
      let queryBay = navigation.state.params.queryType;
      setPageid(pageId);
      console.log(navigation.state.params);
      let url = "https://www.mixesdb.com/db/api.php?";

      data.append("action", "query");
      data.append("prop", "extlinks");
      data.append(queryBay, pageid);
      data.append("formatversion", "2");
      data.append("format", "json");

      await fetch(url, {
        method: "POST",
        body: data,
        mode: "cors"
      })
        .then(json => {
          return json.text();
        })
        .then(data => {
          const obj = JSON.parse(data);
          console.log(obj)
          if (obj.query && obj.query.pages && obj.query.pages[0].extlinks) {
           
            setSource(obj.query.pages[0].extlinks[0].url);
            setLoading(false);
          }
        });
    } catch (err) {
      console.log("Error fetching data-----------", err);
    }
  };

  const displaySpinner = () => {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  };

  if (source) {
    return (
      <WebView
        source={{ uri: source }}
        style={{ marginTop: 20 }}
        startInLoadingState={true}
        renderLoading={() => {
          return displaySpinner();
        }}
      />
    );
  } else {
    return (
      <View style={styles.container}>
        <Text style={styles.message}>Please select a mix set to play</Text>
      </View>
    );
  }
};

const myIcon = <Icon name="play" size={20} color="#fff" />;
Player.navigationOptions = () => ({
  title: "Player",
  tabBarIcon: myIcon,
  headerStyle: {
    backgroundColor: "#f4511e"
  },
  headerTintColor: "#fff",
  headerTitleStyle: {
    fontWeight: "bold"
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#0000",
    alignItems: "center",
    justifyContent: "center"
  },
  spinner: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  message: {
    fontWeight: "bold",
    fontSize: 20
  }
});
